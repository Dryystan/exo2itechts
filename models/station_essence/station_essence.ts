export class StationEssence {
  private _nom: string;
  private _carburants: string[];

  constructor(nom: string, carburants: string[]) {
    this._nom = nom;
    this._carburants = [...carburants];
  }

  get nom(): string {
    return this._nom;
  }
  get carburants(): string[] {
    return [...this._carburants];
  }

  set nom(nom: string) {
    this._nom = nom;
  }
  set carburants(carburants: string[]) {
    this._carburants = [...this.carburants];
  }
}
