import {Vehicule} from "../vehicule";
import {Moteur} from "../../moteurs/moteur";

export class Voiture extends Vehicule {
  public static readonly PLACES: number = 5;
  private _volume_coffre: number;

  constructor(volume_coffre: number, marque: string, couleur: string, prix: number, max_carburant: number, carburant: number, moteur: Moteur, facteur_consommation: number = 1) {
    super(marque, couleur, prix, max_carburant, carburant, moteur, facteur_consommation);
    this._volume_coffre = volume_coffre;
  }

  get volume_coffre(): number {
    return this._volume_coffre;
  }

  set volume_coffre(volume_coffre: number) {
    if(volume_coffre > 0) {
      this._volume_coffre = volume_coffre;
    }
  }

  public details(): string {
    return `Places: ${Voiture.PLACES}, Volume du coffre: ${this._volume_coffre}, ${super.details()}`;
  }
}
