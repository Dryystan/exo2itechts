import {Vehicule} from "../vehicule";
import {Moteur} from "../../moteurs/moteur";

export class Moto extends Vehicule {
  public static readonly PLACES: number = 2;
  private _guidon: string;

  constructor(guidon: string, marque: string, couleur: string, prix: number, max_carburant: number, carburant: number, moteur: Moteur, facteur_consommation: number = 1) {
    super(marque, couleur, prix, max_carburant, carburant, moteur, facteur_consommation);
    this._guidon = guidon;
  }

  get guidon() {
    return this._guidon;
  }

  set guidon(guidon: string) {
    this._guidon = guidon;
  }

  public details(): string {
    return `Places: ${Moto.PLACES}, Guidon: ${this._guidon}, ${super.details()}`;
  }
}
