import {Vehicule} from "../vehicule";
import {Moteur} from "../../moteurs/moteur";

export class Camion extends Vehicule {
  public static readonly PLACES: number = 3;
  private _remorque: boolean = false;
  private _destination: string;

  constructor(destination: string, marque: string, couleur: string, prix: number, max_carburant: number, carburant: number, moteur: Moteur, facteur_consommation: number = 1) {
    super(marque, couleur, prix, max_carburant, carburant, moteur, facteur_consommation);
    this._destination = destination;
  }

  get remorque(): boolean {
    return this._remorque;
  }
  get destination(): string {
    return this._destination;
  }

  set remorque(remorque: boolean) {
    this._remorque = remorque;
  }
  set destination(destination: string) {
    this._destination = destination;
  }

  public details(): string {
    return `Places: ${Camion.PLACES}, Remorque: ${this._remorque ? "Oui" : "Non"}, Destination: ${this._destination}, ${super.details()}`;
  }
}
