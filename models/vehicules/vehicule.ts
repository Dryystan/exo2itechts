import {Moteur} from "../moteurs/moteur";
import {MoteurDiesel} from "../moteurs/diesel/moteur_diesel";
import {MoteurElectrique} from "../moteurs/electrique/moteur_electrique";
import {MoteurEssence} from "../moteurs/essence/moteur_essence";
import {StationEssence} from "../station_essence/station_essence"

export class Vehicule {
  private _marque: string;
  private _couleur: string;
  private _prix: number;
  private _max_carburant: number;
  private _carburant: number;
  private _facteur_consommation = 1;
  private _moteur: Moteur;
  private _panne: boolean = false;

  constructor(marque: string, couleur: string, prix: number, max_carburant: number, carburant: number, moteur: Moteur, facteur_consommation?: number) {
    this._marque = marque;
    this._couleur = couleur;
    this._prix = prix;
    this._moteur = moteur;
    this._max_carburant = max_carburant;
    if(carburant > this._max_carburant) {
      this._carburant = this._max_carburant;
    }
    else {
      this._carburant = carburant;
    }
    if(facteur_consommation) {
      this._facteur_consommation = facteur_consommation;
    }
  }

  get marque(): string {
    return this._marque;
  }
  get couleur(): string {
    return this._couleur;
  }
  get prix(): number {
    return this._prix;
  }
  get max_carburant(): number {
    return this._max_carburant;
  }
  get carburant(): number {
    return this._carburant;
  }
  get moteur(): Moteur {
    return this._moteur;
  }

  set marque(marque: string) {
    this._marque = marque;
  }
  set couleur(couleur: string) {
    this._couleur = couleur;
  }
  set prix(prix: number) {
    this._prix = prix;
  }
  set max_carburant(max_carburant: number) {
    this._max_carburant = max_carburant;
  }
  set carburant(carburant: number) {
    if(carburant > this._max_carburant) {
      this._carburant = this._max_carburant;
    }
    else {
      this._carburant = carburant;
    }
  }
  set moteur(moteur: Moteur) {
    this._moteur = moteur;
  }

  public rouler(distance: number) {
    if(this._panne) {
      console.log('Véhicule en panne... Il faut remplacer le moteur...');
    }
    else {
      if(distance * this._facteur_consommation > this._carburant) {
        this._panne = true;
        console.log('Trop grande distance, véhicule en panne... Il faut remplacer le moteur...');
      }
      else {
        this._carburant -= distance * this._facteur_consommation;
        console.log(`Trajet terminé! Il vous reste ${this._carburant} carburant.`);
      }
    }
  }

  public remplacerMoteur(moteur: Moteur) {
    this._moteur = moteur;
    this._panne = false;
    this._carburant = this._max_carburant;
    console.log(`Nouveau moteur prêt à rouler! ${this._carburant} carburant restant.`);
  }

  public static plein(vehicule: Vehicule, station_essence: StationEssence) {
    let plein = false;
    if(vehicule.moteur instanceof MoteurDiesel) {
      if(station_essence.carburants.indexOf(MoteurDiesel.CARBURANT) >= 0) {
        plein = true;
      }
    }
    else if (vehicule.moteur instanceof MoteurElectrique) {
      if(station_essence.carburants.indexOf(MoteurElectrique.CARBURANT) >= 0) {
        plein = true;
      }
    }
    else if (vehicule.moteur instanceof MoteurEssence) {
      if(station_essence.carburants.indexOf(MoteurEssence.CARBURANT) >= 0) {
        plein = true;
      }
    }
    if(plein) {
      vehicule.carburant = vehicule.max_carburant;
      console.log(`Le plein de ${vehicule.marque} ${vehicule.couleur} (${vehicule.carburant}) a été fait dans la station ${station_essence.nom}`);
    }
    else {
      console.log("Cette station ne permet pas de faire le plein de votre véhicule!");
    }
  }

  public details(): string {
    return `Marque: ${this.marque}, Couleur: ${this.couleur}, Prix: ${this.prix}, Carburant: ${this._carburant}, Max Carburant: ${this._max_carburant}, Facteur de consommation: ${this._facteur_consommation}, Moteur: ${this.moteur.details()}`;
  }
}
