export class Moteur {
  private _vitesse: number;
  private _kilometrage: number;

  constructor(vitesse: number, kilometrage: number) {
    this._vitesse = vitesse;
    this._kilometrage = kilometrage;
  }

  get vitesse(): number {
    return this._vitesse;
  }
  get kilometrage(): number {
    return this._kilometrage;
  }

  set vitesse(vitesse: number) {
    if(vitesse > 0) {
      this._vitesse = vitesse;
    }
  }
  set kilometrage(kilometrage: number) {
    if(kilometrage > 0) {
      this._kilometrage = kilometrage;
    }
  }

  public details(): string {
    return `Vitesse: ${this.vitesse}, Kilometrage: ${this.kilometrage}`;
  }
}
