import {Moteur} from "../moteur";

export class MoteurDiesel extends Moteur {
  public static readonly CARBURANT: string = "DIESEL";

  constructor(vitesse: number, kilometrage: number) {
    super(vitesse, kilometrage);
  }

  public details(): string {
    return `Carburant: ${MoteurDiesel.CARBURANT}, ${super.details()}`;
  }
}
