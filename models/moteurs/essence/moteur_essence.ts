import {Moteur} from "../moteur";

export class MoteurEssence extends Moteur {
  public static readonly CARBURANT: string = "ESSENCE";

  constructor(vitesse: number, kilometrage: number) {
    super(vitesse, kilometrage);
  }

  public details(): string {
    return `Carburant: ${MoteurEssence.CARBURANT}, ${super.details()}`;
  }
}
