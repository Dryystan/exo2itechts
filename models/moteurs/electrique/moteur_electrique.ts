import {Moteur} from "../moteur";

export class MoteurElectrique extends Moteur {
  public static readonly CARBURANT: string = "ELECTRICITE";
  private _voltage: number;

  constructor(voltage: number, vitesse: number, kilometrage: number) {
    super(vitesse, kilometrage);
    this._voltage = voltage;
  }

  get voltage() {
    return this._voltage;
  }

  set voltage(voltage: number) {
    if(voltage > 0) {
      this._voltage = voltage;
    }
  }

  public details(): string {
    return `Carburant: ${MoteurElectrique.CARBURANT}, Voltage: ${this._voltage}, ${super.details()}`;
  }
}
