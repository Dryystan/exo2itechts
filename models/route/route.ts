export class Route {
  private _distance: number;

  constructor(distance: number) {
    this._distance = distance;
  }

  get distance(): number {
    return this._distance;
  }

  set distance(distance: number) {
    this._distance = distance;
  }
}
