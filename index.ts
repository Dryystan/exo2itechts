import {MoteurEssence} from "./models/moteurs/essence/moteur_essence";
import {MoteurDiesel} from "./models/moteurs/diesel/moteur_diesel";
import {MoteurElectrique} from "./models/moteurs/electrique/moteur_electrique";

import {Vehicule} from "./models/vehicules/vehicule";
import {Camion} from "./models/vehicules/camion/camion";
import {Moto} from "./models/vehicules/moto/moto";
import {Voiture} from "./models/vehicules/voiture/voiture";

import {StationEssence} from "./models/station_essence/station_essence";
import {Route} from "./models/route/route";

let camion: Camion = new Camion("Allemagne", "Volvo", "Blanc", 62500, 5000, 4500, new MoteurEssence(150, 15000), 8);
let moto: Moto = new Moto("Chaft", "Kawasaki", "Verte", 7799, 500, 350, new MoteurDiesel(200, 20));
let voiture: Voiture = new Voiture(890, "Audi", "Grise", 82600, 1000, 1000, new MoteurElectrique(140, 200, 0), 2);

let station_essence: StationEssence = new StationEssence("Station1", [MoteurDiesel.CARBURANT, MoteurEssence.CARBURANT, MoteurElectrique.CARBURANT]);
let station_essence2: StationEssence = new StationEssence("Station1", [MoteurElectrique.CARBURANT]);

console.log(camion.details());
console.log(moto.details());
console.log(voiture.details());

Vehicule.plein(voiture, station_essence);

let route: Route = new Route(350);

moto.rouler(route.distance);
moto.rouler(route.distance);
moto.remplacerMoteur(new MoteurEssence(500, 250));
moto.rouler(route.distance);
Vehicule.plein(moto, station_essence2);
Vehicule.plein(moto, station_essence);
moto.rouler(route.distance);
